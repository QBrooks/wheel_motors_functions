# Example 1
#

from __future__ import print_function
import time
#import keyboard
import sys
import math
import qwiic_scmd

myMotor = qwiic_scmd.QwiicScmd()

def runExample():
	print("Motor Test.")
	R_MTR = 0
	L_MTR = 1
	FWD = 0
	BWD = 1
	value = None


	if myMotor.connected == False:
		print("Motor Driver not connected. Check connections.", \
			file=sys.stderr)
		return
	myMotor.begin()
	print("Motor initialized.")
	time.sleep(.250)
	
	# Zero Motor Speeds
	myMotor.set_drive(0,0,0)
	myMotor.set_drive(1,0,0)
	
	myMotor.enable()
	print("Motor enabled")
	time.sleep(.250)


	while True:

		speed = 50
		myMotor.set_drive(R_MTR,FWD,speed)
		myMotor.set_drive(L_MTR,FWD,speed)
		value = input()

		if value == "a":
			print("left sensor-move right")
			#move back for 3 seconds
			myMotor.set_drive(R_MTR,BWD,speed)
			myMotor.set_drive(L_MTR,BWD,speed)
			time.sleep(3)
			#move right for 1 seconds
			myMotor.set_drive(R_MTR,BWD,speed)
			myMotor.set_drive(L_MTR,FWD,speed)
			time.sleep(1)
			#stop
			myMotor.set_drive(R_MTR,FWD,0)
			myMotor.set_drive(L_MTR,FWD,0)
			time.sleep(1)

		if value == "s":
			print("front sensor-move back")
			#move back for 3 seconds
			myMotor.set_drive(R_MTR,BWD,speed)
			myMotor.set_drive(L_MTR,BWD,speed)
			time.sleep(3)
			#stop
			myMotor.set_drive(R_MTR,FWD,0)
			myMotor.set_drive(L_MTR,FWD,0)
			time.sleep(1)

		if value == "d":			
			print("right sensor-move left")
			#move back for 3 seconds
			myMotor.set_drive(R_MTR,BWD,speed)
			myMotor.set_drive(L_MTR,BWD,speed)
			time.sleep(3)
			#move right for 1 seconds
			myMotor.set_drive(R_MTR,FWD,speed)
			myMotor.set_drive(L_MTR,BWD,speed)
			time.sleep(1)
			#stop
			myMotor.set_drive(R_MTR,FWD,0)
			myMotor.set_drive(L_MTR,FWD,0)
			time.sleep(1)

if __name__ == '__main__':
	try:
		runExample()
	except (KeyboardInterrupt, SystemExit) as exErr:
		print("Ending example.")
		myMotor.disable()
		sys.exit(0)
